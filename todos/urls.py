from django.urls import path
from .views import todolist_view, details_view


urlpatterns = [
    path("<int:id>/", details_view, name="todo_list_detail"),
    path("", todolist_view, name="todo_list_list"),
]
