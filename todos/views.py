from django.shortcuts import render, get_object_or_404
from .models import TodoList


# Create your views here.
def todolist_view(request):
    list_view = TodoList.objects.all()
    context = {
        "list_view": list_view,
        }
    return render(request, "todos/list.html", context)


def details_view(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item": details,
    }
    return render(request, "todos/detail.html", context)
